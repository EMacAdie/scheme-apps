#lang simply-scheme
; (require "simply-constants.rkt")
(provide (all-defined-out))

(define CENTURY-SEC 3155760000) 
(define DECADE-SEC 315576000) 
(define YEAR-SEC    31557600)
(define WEEK-SEC 604800)
(define DAY-SEC 86400)
(define HOUR-SEC 3600)
(define MINUTE-SEC 60)

